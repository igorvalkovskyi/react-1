const gulp = require("gulp");
const sass = require('gulp-sass')(require('sass'));

function style(){
    return gulp.src("src/scss/**.scss")
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(gulp.dest("dist/css"));

}
function watch() {
    gulp.watch("src/scss/**.scss", style)
}

exports.style = style;
exports.watch = watch;
