import React from 'react';
import './Modal.scss';


const Modal = ({ header, closeButton, text,text1, actions, actions1,onClose, backgroundColor,backgroundColor2 }) => {
  const closeModal = () => {
    if (onClose) {
      onClose();
    }
  };

  return (
    <div className='popup'>
    <div className="modal-overlay" onClick={closeModal} style={{ backgroundColor }}>
      <div className="modal" style={{ backgroundColor}}>
        
      <h2>{header}</h2>
        {closeButton && (
            <span class="material-symbols-outlined" onClick={closeModal}>
            close
            </span>
    
        )}
        
        </div>
        <div className='modal-all-contents'>
        <div className="modal-content">{text}</div>
        <div className="modal-content1">{text1}</div>
        <div className='buttons-modal-actions'>
            <div className='btn-actions'>
        <div>{actions}</div>
        <div>{actions1}</div>
        </div>
        </div>
        </div>
    </div>
    </div>
  );
};

export default Modal;