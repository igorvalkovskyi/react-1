import React, { useState } from 'react';
import './App.css';
import Button from './Button';
import Modal from './Modal';

function App() {
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);

  return (
    <div className="App">
      <div className='app-btns'>
      <Button
        backgroundColor="#0c9c05"
        text="Open first modal"
        onClick={() => setFirstModalOpen(true)}
      />
      <Button
        backgroundColor="#bd3228"
        text="Open second modal"
        onClick={() => setSecondModalOpen(true)}
      />
</div>
      {firstModalOpen && (
        <Modal
          header="Do you want to update this file?"
          backgroundColor="#61c25d"
          backgroundColor2="#458c42"
          closeButton={true}
          text="Once you update this files, unsaved information will be deleted."
          text1="Are you sure you want to update it?"
          onClose={() => setFirstModalOpen(false)}
          actions={<button className="modal-actions-green" onClick={() => setFirstModalOpen(false)}>Ok</button>}
          actions1={<button className="modal-actions-green1" onClick={() => setFirstModalOpen(false)}>Close</button>}
        />
      )}

      {secondModalOpen && (
        <Modal
          header="Do you want to delete this file?"
          closeButton={true}
          text="Once you delete this file,it won't be possible to undo this action."
          text1="Are you sure you want to delete it?"
          onClose={() => setSecondModalOpen(false)}
          actions={<button className="modal-actions" onClick={() => setSecondModalOpen(false)}>Ok</button>}
          actions1={<button className="modal-actions1"  onClick={() => setSecondModalOpen(false)}>Close</button>}
        
        />
        
      )}
    </div>
  );
}

export default App;